function d_omega = MotorModel(omega_m, throttle)

       %{ 
    if throttle>1
        throttle=1;
    elseif throttle<0
        throttle = 0;
    end
    %}
    motor_idle_RPM = 500;
    throttle2RPM = 5500;
    RPMdemand = RPM2RADs(throttle*throttle2RPM+motor_idle_RPM);        

    d_omega = -10*omega_m+10*RPMdemand;

end