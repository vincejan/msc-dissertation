function [ y ] = Bixler2_g( x, u )
%BIXLER2_G Output of a Bixler2 UAV
%   y - output:
%           alpha
%           beta
%           p
%           q
%           r
%           phi
%           theta
%           psi
%           Vt
%   u - inputs:
%           Ailerons
%           Elevator
%           Rudder
%           Sweep
%           Washout
%           Throttle
%
%   y - output:
%           alpha
%           beta
%           p
%           q
%           r
%           phi
%           theta
%           Vt

    y = x;

end
