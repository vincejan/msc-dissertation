function [Thrust_v,Torque_v] = PropellerBehavior(omega_m)

    omega_m     = -omega_m;
    rho = 1.225;
    %rho = 1.1839;      %air density in Temperature of 25 degrees Celsius
    incline_angle = deg2rad(15);
    
    CT = 0.11;
    CQ = 0.011;
    Dprop = 0.1778;     % 7inch prop= / 6inch prop=0.1524m
    R = (Dprop/2);
    A = (pi*(Dprop/2)^2);
    
    Thrust = CT*rho*A*(omega_m*R)^2;
    Torque = CQ*rho*A*(omega_m*R)^2*R;

    Thrust_v = [(Thrust*cos(incline_angle)) 0 (Thrust*sin(incline_angle))]';
    Torque_v = [(Torque*cos(incline_angle)) 0 (Torque*sin(incline_angle))]';
end