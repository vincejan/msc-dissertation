function TV = ThrustVectoring(thrust,dir)

    TV = [(thrust*cos(dir)) 0 (thrust*sin(dir))]';
    
end