%% Matlab function that first determines all the characteristic aerodynamic coefficients
%% of the Bixler2 swept wing vehicle as a function of it's current airspeed angle of attack
%% and angle of sideslip before computing the full 6-DOF Forces and Moments acting upon the aircraft!

%% Nomenclature

%   V                   Airspeed in m/s
%   alpha               Angle of attack in deg
%   beta                Angle of sideslip in deg
%   q                   Rate of change of alpha in deg/s
%   p                   Rate of change of theta in deg/s
%   r                   Rate of change of beta in deg/s
%   Thrust              Motor generated Thrust in N
%   DELTAelevator       Elevator deflection demand in deg (positive down)
%   DELTAsweep          Symetric wing sweep demand in deg (positive forward)
%   DELTAstarbrdwingtip Starboard wing tip deflection demand in deg (positive leading edge up)
%   DELTAportwingtip    Port wing tip deflection demand in deg (positive leading edge up)
%   DELTArudder         Rudder deflection demand in deg (positive starboard)
%   Cl                  Lift Coefficient
%   Cd                  Drag Coefficient
%   CMx                 Rolling Moment Coefficient
%   CMy                 Pitching Moment Coefficient
%   CMz                 Yawing Moment Coefficient
%   F                   Forces vector applied onto the aircraft
%   M                   Momentrs vector applied onto the aircraft

function [F,M] = BixlerAeroForcesMoments(x,u)

%% Extracting Function Inputs


alpha = rad2deg(x(1));
beta  = rad2deg(x(2));
p     = rad2deg(x(3));
q     = rad2deg(x(4));
r     = rad2deg(x(5));
V     = x(6);
%{
DELTAportwingtip    = threshold(u(1),-1,1)*15;
DELTAstarbrdwingtip =-threshold(u(1),-1,1)*15;
DELTAelevator       =-threshold(u(2),-1,1)*30;
DELTArudder         = threshold(u(3),-1,1)*20;
DELTAsweep          = threshold(u(4),-1/3,1)*30;
DELTAwashout        = threshold(u(5),-1,1/3)*30;
%}

DELTAportwingtip    = u(1)*15;
DELTAstarbrdwingtip =-u(1)*15;
DELTAelevator       =-u(2)*30;
DELTArudder         = u(3)*20;
DELTAsweep          = u(4)*30;
DELTAwashout        = u(5)*30;

% alpha_direction = sign(q);
% q = abs(q);


%% Definition of characteristic Aerodynamic Coefficient Arrays obtained from wind tunnel testing 
%  The tested airspeeds being [14 12 10 8 6] m/s

airspeeds = [14; 12; 10; 8; 6];

% Lift Coefficients

CL0_m5to5 = [0.43; 0.41; 0.39; 0.3771; 0.3417];
CL0_5to10 = [0.593; 0.586; 0.604; 0.6106; 0.634];
CL0_over10 = [1.0313; 1.026; 1.0296; 1.077; 1.103];

CLalpha_m5to5 = [0.0753; 0.0778; 0.0791; 0.08117; 0.085];
CLalpha_5to10 = [0.039; 0.039; 0.037; 0.0354; 0.0299];
CLalpha_over10 = [-0.0048; -0.005; -0.0056; -0.0113; -0.0170];  %Post-Stall


% Dynamic Pitching Lift Coefficient

speed_sample_dynamic = [6; 8; 10];
sweep_sample_dynamic = [0; 10; 20; 30];

% CLq_10ms_0sweep = 0.00555;
CLq_6ms = [0.01012; 0.01217; 0.01005; 0.010104];
CLq_8ms = [0.008201; 0.0081213; 0.008229; 0.008633];
CLq_10ms = [0.00555; 0.0073; 0.007708; 0.006868];


% Drag Coefficients

CD0_m5to5 = [0.084; 0.087; 0.0765; 0.075; 0.0682];
CD0_5to10 = [0.0456; 0.0394; 0.0259; -0.0054; -0.02125];
CD0_over10 = [-0.039; -0.06; -0.0715; -0.0167; 0.0027];

CDalpha_m5to0 = [0.0005; 0.0003; 0.0003; 0; 0.0006];
CDalpha_0to5 = [0.00436; 0.00414; 0.00584; 0.00463; 0.0067];
CDalpha_5to10 = [0.01204; 0.01366; 0.01596; 0.02071; 0.02457];
CDalpha_over10 = [0.0205; 0.0236; 0.0257; 0.02185; 0.02217];    %Post-Stall


% Pitching Moment Coefficients

CMY0_m5to14 = [-0.006; 0.01; 0.02; 0.0215; 0.01045];
CMY0_over14 = [-0.3065; -0.307; -0.323; -0.304; -0.29075];

CMYalpha_m5to0 = [-0.01224; -0.01054; -0.01152; -0.0115; -0.01185];
CMYalpha_0to14 = [-0.02154; -0.02264; -0.0245; -0.02325; -0.02151];
%CMYalpha_over14 = [0; 0; 0; 0; 0];                                   %Post-Stall
CMYalpha_over14 = CMYalpha_0to14;


% Dynamic Pitching Moment Coefficient

CMYq_6ms = [-0.0024608; -0.0033244; -0.003728; -0.0046806];
CMYq_8ms = [-0.002354; -0.00269025; -0.0030326; -0.0038794];
CMYq_10ms = [-0.002311; -0.0023768; -0.002771; -0.0029218];


% Elevator effectiveness Pitching Moment Coefficient

alpha_sample_elev = [-5; -2.5; 0; 2.5; 5];

CMYelev_8p5to4p4 = [-0.00480; -0.00523; -0.006512; -0.004707; -0.00573];
CMYelev_4p4to1p8 = [-0.01088; -0.01069; -0.011315; -0.01111; -0.01004];
CMYelev_1p8tom1p2 = [-0.00533; -0.0085; -0.008993; -0.0099; -0.0103];
CMYelev_m1p2tom5 = [-0.002184; -0.004632; -0.006184; -0.005978; -0.006237];

CMYelev_overall = [-0.005355; -0.0068404; -0.0073926; -0.00745304; -0.0077259];


% Rudder effectiveness Yawing Moment Coefficient

alpha_sample_rudd = [-5; -2.5; 0; 2.5; 5];

CMZrudd_20to10 = [0.0003325; 0.0002946; 0.0002567; 0.000308; 0.00036241];
CMZrudd_10to0 = [0.0003961; 0.00038266; 0.00036924; 0.000399; 0.00042889];
CMZrudd_0tom10 = [0.0003996; 0.0003719; 0.00034422; 0.0003965; 0.0004488];
CMZrudd_m10tom20 = [0.00025022; 0.0002496; 0.00024895; 0.00031153; 0.0003741];


% Wing Sweep effectiveness Pitching Moment Coefficient

speed_sample_sweep = [6; 8; 10; 12; 14];
alpha_sample_sweep = [-5; 0; 5; 10; 13; 15; 20; 25];

% CMYsweep_8ms_5aoa = 0.011973;
CMYsweep_6ms = [-0.00302; 0.000745; 0.003357; 0.00472; 0.005048; 0.00518; 0.00528; 0.00573];
CMYsweep_8ms = [-0.00221; 0.00363; 0.00837; 0.01157; 0.01191; 0.0107; 0.0124; 0.0131];
CMYsweep_10ms = [-0.00083; 0.00889; 0.01672; 0.02257; 0.02328; 0.02455; 0.0236; 0.0278];
CMYsweep_12ms = [-0.00125; 0.0140; 0.02631; 0.03646; 0.03951];
CMYsweep_14ms = [-0.00339; 0.01116; 0.02494];


% Wing Tip Washout effectiveness Pitching Moment Coefficient

sweep_sample_washout = [10; 20; 30];
alpha_sample_washout = [-5; 0; 5; 10; 15; 20; 25];

CMYwashout_0sweep_m30tom10deflec = [-0.00331; -0.003047; -0.00362; -0.00428; -0.00511; -0.001305; -0.000811];
CMYwashout_0sweep_m10to5deflec = [0.01109; 0.009013; 0.01248; 0.01463];

CMYwashout_10sweep = [0.00358; 0.00492; 0.0072; 0.009088; 0.00869; 0.009086; 0.01204];
CMYwashout_20sweep = [0.01247; 0.0134; 0.01282; 0.01429; 0.01214; 0.01406; 0.01528];
CMYwashout_30sweep = [0.01679; 0.01582; 0.01279; 0.01254; 0.01307; 0.01519; 0.01659];


% Wing Tip Port and Starboard effectiveness Rolling Moment Coefficient

sweep_sample_wingtip = [-10; 0; 10; 20];
alpha_sample_wingtip = [-5; 0; 5; 10; 15; 20; 25];

CMXportwingtip_m10sweep = [0.0230; 0.02153; 0.022415; 0.016924; 0.01618; 0.011757; 0.010183];
CMXportwingtip_0sweep = [0.026895; 0.02629; 0.02659; 0.02242; 0.018992; 0.01786; 0.01563];
CMXportwingtip_10sweep = [0.02987; 0.03098; 0.0323; 0.03146; 0.0292; 0.02529; 0.01958];
CMXportwingtip_20sweep = [0.0289; 0.0304; 0.03098; 0.0225; 0.03213; 0.02575; 0.0293];

CMXstarboardwingtip_m10sweep = [-0.01822; -0.019; -0.020148; -0.02041; -0.02316; -0.02308; -0.018754];
CMXstarboardwingtip_0sweep = [-0.02083; -0.02190; -0.02313; -0.02769; -0.0327; -0.026486; -0.02888];
CMXstarboardwingtip_10sweep = [-0.02062; -0.0253; -0.03089; -0.03504; -0.03468; -0.0349; -0.03094];
CMXstarboardwingtip_20sweep = [-0.02167; -0.02885; -0.02985; -0.02913; -0.03361; -0.027941; -0.027445];




%% Interpolation/Extrapolation of the desired coefficients at the given flight speed and attitude

% Lift Drag & Pitching moment coefficients estimation
if alpha<0
    
    CL0 = interp1(airspeeds,CL0_m5to5,V,'linear','extrap');
    CLalpha = interp1(airspeeds,CLalpha_m5to5,V,'linear','extrap');
    
    CD0 = interp1(airspeeds,CD0_m5to5,V,'linear','extrap');
    CDalpha = interp1(airspeeds,CDalpha_m5to0,V,'linear','extrap');
    
    CMY0 = interp1(airspeeds,CMY0_m5to14,V,'linear','extrap');
    CMYalpha = interp1(airspeeds,CMYalpha_m5to0,V,'linear','extrap');
    
    
elseif (0<=alpha) && (alpha<5)
    
    CL0 = interp1(airspeeds,CL0_m5to5,V,'linear','extrap');
    CLalpha = interp1(airspeeds,CLalpha_m5to5,V,'linear','extrap');
    
    CD0 = interp1(airspeeds,CD0_m5to5,V,'linear','extrap');
    CDalpha = interp1(airspeeds,CDalpha_0to5,V,'linear','extrap');
    
    CMY0 = interp1(airspeeds,CMY0_m5to14,V,'linear','extrap');
    CMYalpha = interp1(airspeeds,CMYalpha_0to14,V,'linear','extrap');
    
    
elseif (5<=alpha) && (alpha<10)
    
    CL0 = interp1(airspeeds,CL0_5to10,V,'linear','extrap');
    CLalpha = interp1(airspeeds,CLalpha_5to10,V,'linear','extrap');
    
    CD0 = interp1(airspeeds,CD0_5to10,V,'linear','extrap');
    CDalpha = interp1(airspeeds,CDalpha_5to10,V,'linear','extrap');
    
    CMY0 = interp1(airspeeds,CMY0_m5to14,V,'linear','extrap');
    CMYalpha = interp1(airspeeds,CMYalpha_0to14,V,'linear','extrap');
    
    
elseif (10<=alpha) && (alpha<14)
    
    CL0 = interp1(airspeeds,CL0_over10,V,'linear','extrap');
    CLalpha = interp1(airspeeds,CLalpha_over10,V,'linear','extrap');
    
    CD0 = interp1(airspeeds,CD0_over10,V,'linear','extrap');
    CDalpha = interp1(airspeeds,CDalpha_over10,V,'linear','extrap');
    
    CMY0 = interp1(airspeeds,CMY0_m5to14,V,'linear','extrap');
    CMYalpha = interp1(airspeeds,CMYalpha_0to14,V,'linear','extrap');
    
    
elseif alpha>=14
    
    CL0 = interp1(airspeeds,CL0_over10,V,'linear','extrap');
    CLalpha = interp1(airspeeds,CLalpha_over10,V,'linear','extrap');
    
    CD0 = interp1(airspeeds,CD0_over10,V,'linear','extrap');
    CDalpha = interp1(airspeeds,CDalpha_over10,V,'linear','extrap');
    
    CMY0 = interp1(airspeeds,CMY0_over14,V,'linear','extrap');
    CMYalpha = interp1(airspeeds,CMYalpha_over14,V,'linear','extrap');
    
end

% Elevator effectiveness coefficient estimation

% if DELTAelevator > 4.4
%     CMYelevator = interp1(alpha_sample_elev,CMYelev_8p5to4p4,alpha,'linear','extrap');
%     
% elseif (4.4>=DELTAelevator) && (DELTAelevator>1.8)
%     CMYelevator = interp1(alpha_sample_elev,CMYelev_4p4to1p8,alpha,'linear','extrap');
%     
% elseif (1.8>=DELTAelevator) && (DELTAelevator>-1.2)
%     CMYelevator = interp1(alpha_sample_elev,CMYelev_1p8tom1p2,alpha,'linear','extrap');
%     
% elseif DELTAelevator <= -1.2
%     CMYelevator = interp1(alpha_sample_elev,CMYelev_m1p2tom5,alpha,'linear','extrap');
% end

%Test:
        CMYelevator = interp1(alpha_sample_elev,CMYelev_overall,alpha,'linear','extrap');


% Rudder effectiveness coefficient estimation

% CMZ0 = 0.0020716;

if DELTArudder > 10
    CMZrudder = interp1(alpha_sample_rudd,CMZrudd_20to10,alpha,'linear','extrap');
    
elseif (10>=DELTArudder) && (DELTArudder>0)
    CMZrudder = interp1(alpha_sample_rudd,CMZrudd_10to0,alpha,'linear','extrap');
    
elseif (0>=DELTArudder) && (DELTArudder>-10)
    CMZrudder = interp1(alpha_sample_rudd,CMZrudd_0tom10,alpha,'linear','extrap');
    
elseif DELTArudder <= -10
    CMZrudder = interp1(alpha_sample_rudd,CMZrudd_m10tom20,alpha,'linear','extrap');
end


% Wing Sweep effictiveness coefficient estimation

% CMYsweep = CMYsweep_8ms_5aoa;
CMYsweep6 = interp1(alpha_sample_sweep,CMYsweep_6ms,alpha,'linear','extrap');
CMYsweep8 = interp1(alpha_sample_sweep,CMYsweep_8ms,alpha,'linear','extrap');
CMYsweep10 = interp1(alpha_sample_sweep,CMYsweep_10ms,alpha,'linear','extrap');
CMYsweep12 = interp1(alpha_sample_sweep(1:5),CMYsweep_12ms,alpha,'linear','extrap');
CMYsweep14 = interp1(alpha_sample_sweep(1:3),CMYsweep_14ms,alpha,'linear','extrap');

CMYsweep_speeds = [CMYsweep6; CMYsweep8; CMYsweep10; CMYsweep12; CMYsweep14];
CMYsweep = interp1(speed_sample_sweep,CMYsweep_speeds,V,'linear','extrap');


% Wing Tip Symetric Washout effictiveness coefficient estimation

if DELTAsweep < 5
    if DELTAwashout <= -10
        CMYwashout = interp1(alpha_sample_washout,CMYwashout_0sweep_m30tom10deflec,alpha,'linear','extrap');
    elseif DELTAwashout > -10
        CMYwashout = interp1(alpha_sample_washout(4:7),CMYwashout_0sweep_m10to5deflec,alpha,'linear','extrap');
    end
    
elseif DELTAsweep >= 5
    CMYwashout10 = interp1(alpha_sample_washout,CMYwashout_10sweep,alpha,'linear','extrap');
    CMYwashout20 = interp1(alpha_sample_washout,CMYwashout_20sweep,alpha,'linear','extrap');
    CMYwashout30 = interp1(alpha_sample_washout,CMYwashout_30sweep,alpha,'linear','extrap');
    CMYwashout_sweeps = [CMYwashout10; CMYwashout20; CMYwashout30];
    
    CMYwashout = interp1(sweep_sample_washout,CMYwashout_sweeps,DELTAsweep,'linear','extrap');
end


% Wing Tip Port side Rolling Moment effictiveness coefficient estimation

if (alpha+DELTAportwingtip) >= 15
    
    CMXportwingtip = 0;
    
else
    CMXportwingtipM10 = interp1(alpha_sample_wingtip,CMXportwingtip_m10sweep,alpha,'linear','extrap');
    CMXportwingtip0 = interp1(alpha_sample_wingtip,CMXportwingtip_0sweep,alpha,'linear','extrap');
    CMXportwingtip10 = interp1(alpha_sample_wingtip,CMXportwingtip_10sweep,alpha,'linear','extrap');
    CMXportwingtip20 = interp1(alpha_sample_wingtip,CMXportwingtip_20sweep,alpha,'linear','extrap');
    CMXportwingtip_sweeps = [CMXportwingtipM10; CMXportwingtip0; CMXportwingtip10; CMXportwingtip20];
    
    CMXportwingtip = interp1(sweep_sample_wingtip,CMXportwingtip_sweeps,DELTAsweep,'linear','extrap');
end

if (alpha+DELTAstarbrdwingtip) >= 15
    
    CMXstarboardwingtip = 0;
    
else
    CMXstarboardwingtipM10 = interp1(alpha_sample_wingtip,CMXstarboardwingtip_m10sweep,alpha,'linear','extrap');
    CMXstarboardwingtip0 = interp1(alpha_sample_wingtip,CMXstarboardwingtip_0sweep,alpha,'linear','extrap');
    CMXstarboardwingtip10 = interp1(alpha_sample_wingtip,CMXstarboardwingtip_10sweep,alpha,'linear','extrap');
    CMXstarboardwingtip20 = interp1(alpha_sample_wingtip,CMXstarboardwingtip_20sweep,alpha,'linear','extrap');
    CMXstarboardwingtip_sweeps = [CMXstarboardwingtipM10; CMXstarboardwingtip0; CMXstarboardwingtip10; CMXstarboardwingtip20];
    
    CMXstarboardwingtip = interp1(sweep_sample_wingtip,CMXstarboardwingtip_sweeps,DELTAsweep,'linear','extrap');
end


% Dynamic Pitching Lift Coefficient estimation

% CLq = CLq_10ms_0sweep;
%if q < 0
if abs(q) < 0
    CLq = 0;
else
    CLq6 = interp1(sweep_sample_dynamic,CLq_6ms,DELTAsweep,'linear','extrap');
    CLq8 = interp1(sweep_sample_dynamic,CLq_8ms,DELTAsweep,'linear','extrap');
    CLq10 = interp1(sweep_sample_dynamic,CLq_10ms,DELTAsweep,'linear','extrap');

    CLq_speeds = [CLq6; CLq8; CLq10];
    CLq = interp1(speed_sample_dynamic,CLq_speeds,V,'linear','extrap');
end


% Dynamic Pitching Moment Coefficient estimation

CMYq6 = interp1(sweep_sample_dynamic,CMYq_6ms,DELTAsweep,'linear','extrap');
CMYq8 = interp1(sweep_sample_dynamic,CMYq_8ms,DELTAsweep,'linear','extrap');
CMYq10 = interp1(sweep_sample_dynamic,CMYq_10ms,DELTAsweep,'linear','extrap');

CMYq_speeds = [CMYq6; CMYq8; CMYq10];
CMYq = interp1(speed_sample_dynamic,CMYq_speeds,V,'linear','extrap');




%% Aircraft Data Parameters

rho = 1.225;% Air Density
% V = 10;      % m/s
S = 0.26;   % m^2   wing surface (according to the internet!!!)
c = 0.2;    % m     wing chord
g = 9.81;   % m/s^2 gravity


%% Calculating Full Linearised Coefficients, Forces and Moments Applied onto the Aircraft

% Pitch rate orientation
% q = q*alpha_direction;

% Lift Force
Cl = CL0 + CLalpha*alpha + CLq*q;
L = (0.5*Cl*rho*V^2*S);

% Drag Force
Cd = CD0 + CDalpha*alpha;
D = (0.5*Cd*rho*V^2*S);

% Pitching Moment

% CMYq = -0.0023768;
CMy = CMY0 + CMYalpha*alpha + CMYelevator*DELTAelevator + CMYsweep*DELTAsweep +CMYwashout*DELTAwashout + CMYq*q;
Pitch = (0.5*CMy*rho*V^2*S*c);

% Rolling Moment
CMX0 = 0;
CMXp = -0.002;
CMx = CMX0 + CMXstarboardwingtip*DELTAstarbrdwingtip + CMXportwingtip*DELTAportwingtip + CMXp*p;   % + CMXalpha*alpha
% CMx = 0;
Roll = (0.5*CMx*rho*V^2*S*c);

% Yawing Moment
CMZ0 = 0;
CMZr = -0.0005;
CMz = CMZ0 + CMZrudder*DELTArudder + CMZr*r; % + CMZalpha*alpha
% CMz = 0;
Yaw = (0.5*CMz*rho*V^2*S);

% Forces and Moments Vectors

F = [-D; 0; -L];
M = [Roll; Pitch; Yaw];

Output = [F;M]; 
    
    


end