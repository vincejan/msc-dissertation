airspeeds = [14; 12; 10; 8; 6];

CLalpha_m5to5 = [0.0753; 0.0778; 0.0791; 0.08117; 0.085];
CLalpha_5to10 = [0.039; 0.039; 0.037; 0.0354; 0.0299];
CLalpha_over10 = [-0.0048; -0.005; -0.0056; -0.0113; -0.0170];  %Post-Stall

alpha_cl = [-5 5 10];
%alpha_cl = [alpha_cl alpha_cl alpha_cl];
Cl_alpha = [CLalpha_m5to5 CLalpha_5to10 CLalpha_over10];

CMYalpha_m5to0 = [-0.01224; -0.01054; -0.01152; -0.0115; -0.01185];
CMYalpha_0to14 = [-0.02154; -0.02264; -0.0245; -0.02325; -0.02151];
CMYalpha_over14 = [0; 0; 0; 0; 0];                                   %Post-Stall

CMYq_6ms = [-0.0024608; -0.0033244; -0.003728; -0.0046806];
CMYq_8ms = [-0.002354; -0.00269025; -0.0030326; -0.0038794];
CMYq_10ms = [-0.002311; -0.0023768; -0.002771; -0.0029218];

speed_sample_dynamic = [6; 8; 10];
q = linspace(-10,10);

Cm_q = [CMYq_6ms(1) CMYq_8ms(1) CMYq_10ms(1)];

%plot(alpha_cl,Cl_alpha);

plot(q,q'*Cm_q(1),'r','LineWidth',1);
hold on
plot(q,q'*Cm_q(2),'c','LineWidth',1);
plot(q,q'*Cm_q(3),'g','LineWidth',1);
legend('6m/s','8m/s','10m/s');
xlabel('q (deg/s)')
ylabel('C_M')
%plot(q,Cm_q*q');
axis square
grid on
print('cm-q','-dpng');