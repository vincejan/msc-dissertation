function [ d_x ] = Bixler2_f(x, u, constraints)
%BIXLER2_F Dynamics of a Bixler2 UAV
%   x - states:
%           alpha
%           beta
%           p
%           q
%           r
%           phi
%           theta
%           psi
%           Vt
%           omega_m
%
%   u - inputs:
%           Ailerons
%           Elevator
%           Rudder
%           Sweep
%           Washout
%           Throttle
%
%   x_dot - derivative of the state

    m = 1.285;
    I = diag([0.042, 0.029, 0.066]);

    alpha   = x(1,1);
    beta    = x(2,1);
    omega   = x(3:5,1);
    rpy     = x(6:8,1);
    Vt      = x(9,1);
    omega_m = x(10,1);
    
    if Vt == 0
        %dummy case
        %warning('Vt == 0');
        %Vt = 10;
    end
    
    throttle = u(6,1);
    magic_force  = u(6,1);
    magic_moment = u(7,1);
    
    if omega_m == 0
        %warning('Vt == 0');
        %omega_m = RPM2RADs(500);
    end
    
    
    
    Vb  = AlphaBetaV2Airvelocity(alpha,beta,Vt);
    Rwb = RPY(0,alpha,-beta);
    Reb = RPY(rpy(1),rpy(2),rpy(3));
    
    % BixlerForcesMoments requires stuff in RADIANS (conversion is done in
    % the fuction)
    % Returns the forces, moments in wind axis
    [F_aero,M_aero] = BixlerAeroForcesMoments([alpha;beta;omega;Vt],u);
    F_aerob = Rwb\F_aero;
    M_aerob = Rwb\M_aero;
        
    g = [0;0;9.81];
    F_gb = Reb\g*m;
    
    d_omega_m = MotorModel(omega_m,throttle);
    [F_thrust,T_prop] = PropellerBehavior(omega_m);
    %[F_thrust] = ThrustVectoring(magic_force,thrust_dir)
    CG2Prop = [-0.193 0 -0.085]';    
    M_thrust = cross(CG2Prop,F_thrust);    
    
    F_sum = constraints(:,1).*(F_aerob + F_thrust*0 + F_gb + [magic_force;0;0]);
    M_sum = constraints(:,2).*(M_aerob + M_thrust*0 + T_prop*0 + 0*[0;magic_moment;0]);
    
    % d_rates, d_velocities
    d_Vb    = m\F_sum - cross(omega,Vb);
    d_omega = I\(M_sum - cross(omega,I*omega)); 
    
    d_Vt    = (Vb'*d_Vb)/Vt;
    d_alpha = (Vb(1)*d_Vb(3)-Vb(3)*d_Vb(1))/(Vb(1)^2+Vb(3)^2);
    d_beta  = (d_Vb(2)*Vt-Vb(2)*d_Vt)/(Vt^2*cos(beta));
    
    d_rpy   = omegab2dRPY(omega,rpy);
        
    d_x = [d_alpha;d_beta;d_omega;d_rpy;d_Vt;d_omega_m*0];

end

