function dx=Pend_f(x,u)

    nu = 1;
    
    dx=[x(2) ; -10*sin(x(1))-nu*x(2)] +[0;1]*u;
    
    %A = [0 1; -10 -1];
    %B = [0;1];
    %display(num2str(x'))
    %display(num2str(u'))
    %dx = A*x+B*u;
    
end