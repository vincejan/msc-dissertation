clear
pend_enab = false;
bix_enab = true;

% alpha, q, theta, Vt, omega_m, error_theta_int
%% setup gain calc
DOFx = 3;
DOFy = 1;
DOFu = 1;
DOFK = DOFx+DOFy;
DOFeig = 4;

DEig = zeros(0,DOFeig);
DEig(end+1,:) = [-11 -12 -13 -14];
%DEig(end+1,:) = [-2+10i -2-10i -5];
%DEig(end+1,:) = [-.1+0.1i -.1-0.1i -5];

%DEig(end+1,:) = [-3 -4 -100];
%DEig(end+1,:) = [-2+10i -2-10i -100];
%DEig(end+1,:) = [-.1+0.1i -.1-0.1i -100];


%DEig(end+1,:) = [-1+10i -1-10i -5];



X = nan;
DVec = [1 X X X X;
        X 1 X X X;
        X X 1 X X;
        X X X 1 X;
        X X X X 1];
DVec = DVec(1:4,1:4);
    
x0 = zeros(DOFx+DOFy,1);
u0 = zeros(DOFu,1);
    
for gain_id=1:size(DEig,1)
    
    test_run = 1;

    

%% setup sim 

    sim_id = 1;
    
    t_max = 2;
    tspan = [0 t_max];

    
    % alpha, q, theta, Vt, omega_m, error_theta_int
    x0_sim = zeros(DOFx+DOFy,1);
    x0_sim(4) = 10; %m/s

    x_d = zeros(DOFy,1);
    
%% Bixler system
    if bix_enab
        
        x_d(1)=deg2rad(3);
        
        %% base system
        reduced_idx = [1 2 3];
        % gains
            K_base = BaseGain(DVec(reduced_idx,reduced_idx),DEig(gain_id,reduced_idx),x0(reduced_idx(1:2)),u0);
        % sim
            Sim = @(t,x) Base_cont(t,x,0,0,K_base,x_d);
            [to_bix_base,xo_bix_base] = ode45(Sim, tspan, x0_sim(reduced_idx));

        
        
        % gains
            alpha_min = deg2rad(-10);
            alpha_max = deg2rad( 30);
            [Ks_bix,Phis_bix,u_trims_bix,alphas_bix] = BixlerAlphaSweep(DVec,DEig(gain_id,:),x0(1:DOFx),u0,alpha_min,alpha_max);
            K0_bix = interp1(alphas_bix,Ks_bix,0,'spline','extrap');
        % sim SGS
            Sim = @(t,x) Bixler2_scheduled(t,x,alphas_bix,u_trims_bix,K0_bix,Ks_bix,x_d);
            [to_bs,xo_bs] = ode45(Sim, tspan, x0_sim);
            Ko_bs = repmat(K0_bix,length(xo_bs),1);
            Ko_bs(:,1) = interp1(alphas_bix,Ks_bix(:,1),xo_bs(:,1),'spline','extrap');
            uo_bs = -sum(xo_bs.*Ko_bs,2);
        % sim DGS
            Sim = @(t,x) Bixler2_scheduled(t,x,alphas_bix,u_trims_bix,K0_bix,Phis_bix,x_d);
            [to_bd,xo_bd] = ode45(Sim, tspan, x0_sim);
            Ko_bd = repmat(K0_bix,length(xo_bd),1);
            Ko_bd(:,1) = interp1(alphas_bix,Phis_bix(:,1),xo_bd(:,1),'spline','extrap');
            uo_bd = -sum(xo_bd.*Ko_bd,2);
    end
%% plot
    
    
    
    %% Bixler 
    if bix_enab
        %% gains
        
        c = lines;
        
        figure('units','normalized','position',[.0 .0 .75 .75])
        legendInfo = {};

        %alphas_bix_plot= deg2rad(linspace(-10,30,1000));
        %Ks_bix_plot = interp1(alphas_bix,Ks_bix(:,1),alphas_bix_plot','linear','extrap');
        %Phis_bix_plot = interp1(alphas_bix,Phis_bix(:,1),alphas_bix_plot','linear','extrap');
        
        
        plots = 0;
        for k = 1:DOFu
            for l = 1:1%DOFK
                plots = plots+1;
                plot(rad2deg(alphas_bix),Ks_bix(:,l),'x-','Color',c(plots,:),'LineWidth',1)
                %plot(rad2deg(alphas_bix_plot),Ks_bix_plot,'-','Color',c(plots,:));
                hold on
                legendInfo{plots} = ['K - ' num2str(k) ',' num2str(l)]; %num2str(ceil(k/DOFx)) num2str(mod(k,DOFu))]; 
            end
        end

        for k = 1:DOFu
            for l = 1:1%DOFK
                plots = plots+1;
                plot(rad2deg(alphas_bix),Phis_bix(:,l),'x-','Color',c(plots,:),'LineWidth',1)
                %plot(rad2deg(alphas_bix_plot),Phis_bix_plot,'-','Color',c(plots,:));
                hold on
                legendInfo{plots} = ['\Phi - ' num2str(k) ',' num2str(l)]; %num2str(ceil(k/DOFx)) num2str(mod(k,DOFu))]; 
            end
        end

        grid on
        xlabel('\alpha (deg)')
        ylabel('gain magnitude')
        legend(legendInfo)
        print(['alphasweep_bix_' num2str(gain_id)],'-dpng')

        figure('units','normalized','position',[.0 .0 .75 .75])
        plot(rad2deg(alphas_bix),u_trims_bix(:,:,1),'LineWidth',1)
        grid on
        xlabel('\alpha (deg)')
        ylabel('Magnitude')
        legend('u_0');
        print(['u0_bix_' num2str(gain_id)],'-dpng')


        %% sim
        c = lines;
        figure('units','normalized','position',[.0 .0 .75 .75])
        hold on
        grid on

        state2plot = 3;
        % baseline
            plot(to_bix_base,rad2deg(xo_bix_base(:,1)),':','Color',c(1,:),'LineWidth',1)
            %plot(to_bix_base,rad2deg(xo_bix_base(:,2)),':','Color',c(2,:))

        % sys SGS
            plot(to_bs,rad2deg(xo_bs(:,state2plot)),'--','Color',c(1,:),'LineWidth',1)
            plot(to_bs,rad2deg(xo_bs(:,1)),'--','Color',c(2,:))

        % sys DGS
            plot(to_bd,rad2deg(xo_bd(:,state2plot)),'-.','Color',c(1,:),'LineWidth',1)
            plot(to_bd,rad2deg(xo_bd(:,1)),'-.','Color',c(2,:))



        xlabel('time (s)')
        ylabel('magnitude')
        %legend('\alpha_{ideal} (deg)', 'q_{ideal} (deg/s)','\alpha_{SGS} (deg)', 'q_{SGS} (deg/s)','\alpha_{DGS} (deg)', 'q_{DGS} (deg/s)')
        legend('\alpha_{ideal} (deg)','\alpha_{SGS} (deg)','\alpha_{DGS} (deg)')
        grid on
        print(['sim_result_bix_' num2str(gain_id) '_' num2str(sim_id)],'-dpng')
        %title(['Bixler test run for \lambda\in\{' num2str(DEig) '\}']);
        
        
        
        figure('units','normalized','position',[.0 .0 .75 .75])
        hold on
        grid on

        % u
            %plot(to_bs,(uo_bs(:,1)),'--','Color',c(1,:))
            plot(to_bd,(uo_bd(:,1)),'-','Color',c(1,:),'LineWidth',1)



        xlabel('time (s)')
        ylabel('magnitude')
        %legend('\alpha_{ideal} (deg)', 'q_{ideal} (deg/s)','\alpha_{SGS} (deg)', 'q_{SGS} (deg/s)','\alpha_{DGS} (deg)', 'q_{DGS} (deg/s)')
        legend('u_{DGS}')
        grid on
        print(['sim_result_bix_u_' num2str(gain_id) '_' num2str(sim_id)],'-dpng')
        %title(['Pendulum test run for \lambda\in\{' num2str(DEig) '\}']);
    end
    
    save(['run_' num2str(gain_id) '_' num2str(sim_id)])
    %return
    close all
end    
%% save    
    !mv ./*.png ./Results3/
    !mv ./*.mat ./Results3/
        
        



