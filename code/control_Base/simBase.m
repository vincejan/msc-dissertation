clear
num = 310;
t_max = 20;
tspan = [0 t_max];

DOFx = 2;
DOFy  = 1;
x0 = zeros(DOFx+DOFy,1);
% alpha, q
x0(1) = deg2rad(10);

x_d = zeros(DOFy,1);
x_d(1)=deg2rad(100);

%load('alphas.mat');
load('K0.mat');
%load('Ks.mat');
%load('Phis.mat');
%load('u_trims.mat');

%% sim
Sim = @(t,x) Base_cont(t,x,0,0,Kgain0,x_d);
[t1,xout1] = ode45(Sim, tspan, x0);
%xout1 = xout1';

%Sim = @(t,x) Pend_scheduled(t,x,alphas,u_trims,Phis,Kgain0,[x_d;0]);
%[t2,xout2] = ode45(Sim, tspan, [x0;0]);
%xout2 = xout2';

%% plot
c = lines;
figure
plot(t1,rad2deg(xout1(:,1)),'--','Color',c(1,:))
hold on
plot(t1,rad2deg(xout1(:,2)),'--','Color',c(2,:))
grid on
return
plot(t2,rad2deg(xout2(:,1)),'-.','Color',c(1,:))
plot(t2,rad2deg(xout2(:,2)),'-.','Color',c(2,:))

plot(tr,rad2deg(xr(:,1)),':','Color',c(1,:))
plot(tr,rad2deg(xr(:,2)),':','Color',c(2,:))

xlabel('time (s)')
ylabel('magnitude')
legend('\alpha_{SGS} (deg)', 'q_{SGS} (deg/s)','\alpha_{DGS} (deg)', 'q_{DGS} (deg/s)','\alpha_{ideal} (deg)', 'q_{ideal} (deg/s)')
grid on
print(['sim_scheduled_' num2str(num)],'-dpng')
!mv ./*.png ./Results/