function d_x = Base_cont(t,x,alphas,u_trims,K0,x_d)
    
    DOFy = length(x_d);
    DOFx = length(x)-DOFy;
    %%
    
    K = K0;
    
    if t>5
        x_d = -x_d;
    end
    
    error = x_d-x(1);
    u = -K*(x);
    
    d_x = Base_2f(x(1:DOFx),u);
    d_xi= error;    
    
    d_x  = [d_x;d_xi];

end