function [Ks,Phis,u_trims,alphas] = PendAlphaSweep(DVec,DEig,x0,u0,alpha_min,alpha_max)

a_steps = 100;
alphas = linspace(alpha_min,alpha_max,a_steps);


DOFx = length(x0);
DOFy = length(DEig)-DOFx;
DOFu = length(u0);
DOFK = DOFx+DOFy;

Ks = zeros(length(alphas),DOFK);
Phis = zeros(length(alphas),DOFK);
u_trims   = zeros(length(alphas),DOFu);


%% K0
%% Operating point
    % alpha, q
    x0 = zeros(DOFx,1);
    x0(1) = 0;
    % elevator, thrust
    u0 = zeros(DOFu,1);

    %[Kgain0,~,~,u_0] = getGainsLQI(@Bixler2_f2,@Bixler2_g2,Cd,x0,u0,Q,R);
    [Kgain0,~,~,u_0] = getGainsIEA(@Pend_f,@Pend_g,x0,u0,DEig,DVec);
    %[Kgain0,~,~,u_0] = getGainsPl(@Pend_f,@Pend_g,x0,u0,DEig);
    

progress = 0;
for a = 1:length(alphas)
        
        progress = progress+1;
        display([num2str(100*progress/(a_steps),'%10.3f') '%']);
        display(['               Alpha: ' num2str(rad2deg(alphas(a)))]);
        
        %% Operating point
        % alpha, q, theta, Vt
        x0 = zeros(DOFx,1);
        x0(1) = alphas(a);
        
        % elevator, thrust
        u0 = zeros(DOFu,1);

        %[Kgains,~,~,u_trims(a,:)] = getGainsLQI(@Bixler2_f2,@Bixler2_g2,Cd,x0,u0,Q,R);
        [Kgains,~,~,u_trims(a,:)] = getGainsIEA(@Pend_f,@Pend_g,x0,u0,DEig,DVec);
        %[Kgains,~,~,u_trims(a,:)] = getGainsPl(@Pend_f,@Pend_g,x0,u0,DEig);
%        Ks(a,:) = permute(Kgains(1,[1 3]),[3 1 2]);
        Ks(a,:) = Kgains;

end
Phis = dgs(Ks,alphas,Kgain0);
%Phis = numint(alphas,Ks,[0 0])./[alphas;alphas]';
%Phis(1,:)=Kgain0;
return
%% save
save('alphas.mat','alphas');

save('u_trims.mat','u_trims');
save('Ks.mat','Ks');
save('Phis.mat','Phis');

%csvwrite('alphasweep.dat',[alphas' u0_s reshape(K_plot,[steps],[])]);

%return
%% plot
cmap = colorcube;

figure
legendInfo = {};

plots = 0;
for k = 1:1%DOFu
    for l = 1:DOFy
        plots = plots+1;
        plot(rad2deg(alphas),Ks(:,l),'x-');%,'Color',cmap(plots,:));
        hold on
        legendInfo{plots} = ['K - ' num2str(k) ',' num2str(l)]; %num2str(ceil(k/DOFx)) num2str(mod(k,DOFu))]; 
    end
end

for k = 1:1%DOFu
    for l = 1:DOFy
        plots = plots+1;
        plot(rad2deg(alphas),Phis(:,l),'x-');%,'Color',cmap(plots,:));
        hold on
        legendInfo{plots} = ['\Phi - ' num2str(k) ',' num2str(l)]; %num2str(ceil(k/DOFx)) num2str(mod(k,DOFu))]; 
    end
end

grid on
xlabel('\alpha (deg)')
ylabel('gain magnitude')
legend(legendInfo)
print(['alphasweep' num2str(num)],'-dpng')

%%
%{
figure
subplot(2,1,1);
plot(Eig_o,'x--');
grid on
legend('\alpha','q');

%figure
subplot(2,1,2);
plot(Eig_K,'x--');
grid on
legend('\alpha','q');
print(['eigenv' num2str(num)],'-dpng')
%}

figure
plot(rad2deg(alphas),u_trims(:,:,1),'b')
hold on
%plot(rad2deg(alphas),u_trims(:,:,2),'r')
grid on
xlabel('\alpha (deg)')
ylabel('Magnitude')
legend('\delta_{e0}','\delta_{t0}');
print(['u0' num2str(num)],'-dpng')


%%

!mv ./*.png ./Results
%notifyMe('Done');









