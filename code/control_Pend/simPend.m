clear
num = 310;
t_max = 20;
tspan = [0 t_max];

DOFx = 2;
DOFu  = 1;
x0 = zeros(DOFx,1);
% alpha, q
x0(1) = deg2rad(10);

x_d = zeros(DOFx,1);
x_d(1)=deg2rad(100);

load('alphas.mat');
load('K0.mat');
load('Ks.mat');
load('Phis.mat');
load('u_trims.mat');

%% sim
Bixler2sim = @(t,x) Pend_scheduled(t,x,alphas,u_trims,Ks,Kgain0,[x_d;0]);
[t1,xout1] = ode45(Bixler2sim, tspan, [x0;0]);
%xout1 = xout1';

Bixler2sim = @(t,x) Pend_scheduled(t,x,alphas,u_trims,Phis,Kgain0,[x_d;0]);
[t2,xout2] = ode45(Bixler2sim, tspan, [x0;0]);
%xout2 = xout2';

%% ref sim

t = linspace(0,t_max,1000);

u_trim = interp1(alphas,u_trims,x_d(1),'linear','extrap')';
[A,B,C,~] = linearizeModel(@Pend_f,@Pend_g,x_d(1:2),u_trim);

K = interp1(alphas,Ks,x_d(1),'linear','extrap');
x_trim = repmat(x_d(1:2)',length(t),1);

[V,L] = eig(A-B*K(1:2));

sys = ss(A-B*K(1:2),eye(2),C,0);

u = 0*repmat(x_d,1,length(t));

[y,tr,xr] = lsim(sys,u,t,x0-x_d);
xr = xr + x_trim;

%% plot
c = lines;
figure
plot(t1,rad2deg(xout1(:,1)),'--','Color',c(1,:))
hold on
plot(t1,rad2deg(xout1(:,2)),'--','Color',c(2,:))

plot(t2,rad2deg(xout2(:,1)),'-.','Color',c(1,:))
plot(t2,rad2deg(xout2(:,2)),'-.','Color',c(2,:))

plot(tr,rad2deg(xr(:,1)),':','Color',c(1,:))
plot(tr,rad2deg(xr(:,2)),':','Color',c(2,:))

xlabel('time (s)')
ylabel('magnitude')
legend('\alpha_{SGS} (deg)', 'q_{SGS} (deg/s)','\alpha_{DGS} (deg)', 'q_{DGS} (deg/s)','\alpha_{ideal} (deg)', 'q_{ideal} (deg/s)')
grid on
print(['sim_scheduled_' num2str(num)],'-dpng')
!mv ./*.png ./Results/