function [K] = ea(A,B,C, DEig, DVec)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Eigenstructure assignment routine - standard output feedback decoupling EA
% First need to calculate the acheivable eigenvectors

I = eye(size(A));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define each Eigenvalue and find the Achievable Eigevector

Va = zeros(size(DVec));

for e = 1:length(DEig)
    
    Li = (DEig(e)*I-A)\B;
        
    nans = isnan(DVec(:,e));
    li = DVec(~nans,e);
    Li_h = Li(~nans,:);

    Va(:,e) = Li*pinv(Li_h.'*Li_h)*Li_h.'*li;
   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The acheivable eigenvectors are then used to find the gain matrix K

V = Va;
T = diag(DEig);
Ac = V*T/V;

K = pinv(B)*(A-Ac)*pinv(C);









