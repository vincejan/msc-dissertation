% *********************************************************************
%   splines.for      p thomas, 02 july 1996
%   spline functions for hhirm aero and engine coefficients
%   per tsagi report no. 15/5dra
% *********************************************************************
%   Altered for Double Precision use 31/5/99 by
%   T.S.Richardson
%   Bristol Uni
% *********************************************************************
%   Altered for MATLAB use 04/9/00 by
%   T.S.Richardson
%   Bristol Uni
% *********************************************************************

    function [z,dz] = dlspl(xx,yy,dd,x)

    if x <= xx(1)           % left linear portion
      z  = yy(1) + dd(1)*(x-xx(1));
      dz = dd(1);
    elseif x >= xx(2)       % right linear portion
      z  = yy(2) + dd(2)*(x-xx(2));
      dz = dd(2);
    else                    % spline between linear portions
      dx = xx(2) - xx(1);
      wr = (x-xx(1))/dx;
      wl = 1.0 - wr;
      dm = (yy(2)-yy(1))/dx;
      z  = 0.5*(yy(2)+yy(1) + (yy(2)-yy(1))*(wr-wl)) + dx*wr*wl*((dd(1)-dm)*wl - (dd(2)-dm)*wr);
      dz = dd(1)*wl*wl + dd(2)*wr*wr + 2.0d+00*wr*wl*(3.0d+00*dm-dd(2)-dd(1));
    end

