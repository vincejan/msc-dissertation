%% setup
A = [ 0  1  0  0;
      0  0  1  0;
      0  0  0  1
     -1 -2 -3 -4];
[V0,L0] = eig(A);

B=[1 0 0;
   0 1 0;
   0 0 1;
   0 0 0];

C=eye(4);

%% eg 1 - not exact definition of eigenvectors
X = nan;
Vd = [1 X 0 0;
      X 1 X X;
      X X 1 X;
      0 0 X 1];
Ld = [-5 -6 -7 -8];

K=ea(A,B,C,Ld,Vd);

[Vr,Lr] = eig(A-B*K)

%% eg 2 - too strict specification
X = nan;
Vd = [-1 1  0 0;
       1 1  0 0;
       X X -1 1;
       0 0  1 1];
Ld = [-5 -6 -7 -8];

K=ea(A,B,C,Ld,Vd);

[Vr,Lr] = eig(A-B*K)

%% eg 3 - complex eigenvalues, real eigenvectors
X = nan;
Vd = [1 X 0 0;
      X 1 X X;
      X X 1 X;
      0 0 X 1];
Ld = [-5-1i -5+1i -7 -8];

K=ea(A,B,C,Ld,Vd);

[Vr,Lr] = eig(A-B*K)

%% eg 4 - complex eigenvalues, real eigenvectors, small rank(B)
B=[0 0 0 1]';
X = nan;
Vd = [1 X X X;
      X 1 X X;
      X X 1 X;
      X X X 1];
Ld = [-5-1i -5+1i -7 -8];

K=ea(A,B,C,Ld,Vd);

[Vr,Lr] = eig(A-B*K)






