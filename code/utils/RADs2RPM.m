function rpm = RADs2RPM(rads)
    rpm = rads/((360/60)*(pi/180));
end