% *********************************************************************
%   splines.for      p thomas, 02 july 1996
%   spline functions for hhirm aero and engine coefficients
%   per tsagi report no. 15/5dra
% *********************************************************************
%   Altered for Double Precision use 31/5/99 by
%   T.S.Richardson
%   Bristol Uni
% *********************************************************************
%   Altered for MATLAB use 04/9/00 by
%   T.S.Richardson
%   Bristol Uni
% *********************************************************************

    function [z,dz] = bell(xx,yy,x)
      
	 [z1,dz1] = ddspl(xx(1),xx(2),0.0, yy(1),x);
	 [z2,dz2] = ddspl(xx(2),xx(3),0.0,-yy(1),x);
   
    z  = z1  + z2;
    dz = dz1 + dz2;