function Phi = dgs(K,x,K0)

    if x(1)<0
        
        x_n   = [x(x<0) 0];
        %K_n   = [K(:,:,x<0);K0];
        K_n   = cat(3,K(:,:,x<0),K0);
        
        x_p   = [0 x(x>0)];
        %K_p   = [K0;K(:,:,x>0)];
        K_p   = cat(3,K0,K(:,:,x>0));
        
        Phi_n =  numint(-fliplr(x_n),-flip(K_n,3));%./[alphas;alphas]';
        Phi_n =  flip(Phi_n,3);
        Phi_p =  numint(x_p,K_p);%./[alphas;alphas]';
        
        
        Phi = cat(3,Phi_n(:,:,1:end-1),Phi_p(:,:,2:end));
        for i=1:size(K,3)
            Phi(:,:,i)=Phi(:,:,i)./x(i)';
        end
        
    else
        Phi = numint(x,K);
        
        for i=1:size(K,3)
            Phi(:,:,i)=Phi(:,:,i)./x(i)';
        end
        
        Phi(:,:,1)=K0;
    end

end