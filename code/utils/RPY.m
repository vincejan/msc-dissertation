function R=RPY(phi,theta,psi)
%function to calculate the roll, pitch, yaw matrix

    R=  [cos(psi) -sin(psi) 0;
        sin(psi) cos(psi) 0;
        0 0 1]*...
        [cos(theta) 0 sin(theta);
        0 1 0;
        -sin(theta) 0 cos(theta)]*...
        [1 0 0;
        0 cos(phi) -sin(phi);
        0 sin(phi) cos(phi)];

end