function [Kgains,Eig_o,Eig_K,u0] = getGainsIEA(f,g,x0,u0,DEig,DVec)

    %% trim 
    toTrim = @(u) norm(f(x0,u));
    %u0 = fminsearch(toTrim, u0);
    
    %% Linearize
    [A,B,C,~] = linearizeModel(f,g,x0,u0);

    DOFx = size(A,1);
    DOFy = size(C,1);
    DOFu = size(B,2);
    
    Aa = [ A    zeros(DOFx ,DOFy);
          -C zeros(DOFy,DOFy)];
    Ba = [B;zeros(DOFy,DOFu)];
    %Ca = [C zeros(DOFy,DOFy)];
    %C = diag(C);
    C( :, ~any(C,1) ) = [];
    Ca = blkdiag(eye(DOFx+DOFy));
    %Ca = [eye(DOFx) C];
    
    
    
    Co = ctrb(Aa,Ba);
    % Number of uncontrollable states
    unco = length(Aa)-rank(Co);

    Ob = obsv(Aa,Ca);
    % Number of unobservable states
    unob = length(Aa)-rank(Ob);
    
    %% EA
    
    Kgains = ea(Aa,Ba,Ca,DEig,DVec);
    %imag(Kgains)
    Kgains=real(Kgains);
    
    Eig_o = eig(Aa);
    [V_K,Eig_K] = eig(Aa-Ba*Kgains*Ca);
    %Eig_K(1:2)
    %Eig_K(3:end)
end