function [ K ] = EA_out( A,B,C,l_d,V_d )
%EA_FULL Summary of this function goes here
%   Detailed explanation goes here


    DOFx = length(A);
    
    Co = ctrb(A,B);
    % Number of uncontrollable states
    unco = DOFx-rank(Co);

    Ob = obsv(A,C);
    % Number of unobservable states
    unob = DOFx-rank(Ob);

    assert(unco==0,'System is not controllable')
    assert(unob==0,'System is not observable')

    
    [U,R] = qr(B);
    [n,m]=size(B);
    
    % We need U1
    U_0=U(1:n,1:m);
    U_1=U(1:n,(m+1):n);
    Z_B=R(1:m,1:m);
    
    eigs = length(l_d);
    V_a = zeros(size(V_d));
    for ii=1:eigs
        
        %{
        L = (l_d(ii)*eye(DOFx)-A)\B;
        
        nans = isnan(V_d(:,ii));
        l = V_d(~nans,ii);
        L_h = L(~nans,:);
                
        V_a(:,ii) = L*pinv(L_h.'*L_h)*L_h.'*l;
        %}
        
        S = null( transpose(U_1)*(A-l_d(ii)*eye(DOFx)) );
        
        nans = isnan(V_d(:,ii));
        l = V_d(~nans,ii);
        L = S(~nans,:);
        D = S(nans,:);
        
        V_a(:,ii) = S*pinv(L)*l;
    end
    
    L_d = diag(l_d);
    %K = Z_B\U_0.'*(V_a*L_d-A*V_a)*pinv(C*V_a);
    K = pinv(Z_B)*transpose(U_0)*(V_a*L_d-A*V_a)*pinv(C*V_a);
end

