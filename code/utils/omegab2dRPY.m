function [d_rpy] = omegab2dRPY(omega,rpy)

    phi     = rpy(1);
    theta   = rpy(2);
    %psi     = rpy(3);
    RRM = [ 1 sin(phi)*tan(theta) cos(phi)*tan(theta);
            0 cos(phi) -sin(phi);
            0 sin(phi)/cos(theta) cos(phi)/cos(theta)];

    d_rpy = RRM*omega;
end