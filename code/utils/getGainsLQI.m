function [Kgains,Eig_o,Eig_K,u0] = getGainsLQI(f,g,x0,u0,Q,R)

    %% trim 
    toTrim = @(u) norm(f(x0,u));
    %u0 = fminsearch(toTrim, u0);
    
    %% Linearize
    [A,B,C,D] = linearizeModel(f,g,x0,u0);
    
    
    %% LQI
    DOFx = size(A,1);
    DOFy = length(Q)-DOFx;
    %DOFxi= size(Cd,1);
    DOFu = size(B,2);
    
    
    
    Aa = [ A    zeros(DOFx ,DOFy);
          -C zeros(DOFy,DOFy)];
    Ba = [B;zeros(DOFy,DOFu)];
    %Ca = [C zeros(DOFy,DOFy)];
    %C = diag(C);
    %C( :, ~any(C,1) ) = [];
    Ca = blkdiag(eye(DOFx+DOFy));
    %Ca = [eye(DOFx) C];
    
    
    Co = ctrb(Aa,Ba);
    % Number of uncontrollable states
    unco = length(Aa)-rank(Co);

    %Ob = obsv(A,C);
    % Number of unobservable states
    %unob = DOFx-rank(Ob);

    %assert(unco==0,'System is not controllable')
    %assert(unob==0,'System is not observable')
    
    %[Kgains,~,~] = lqr(Aa,Ba,Q,R);
    [Kgains,~,~] = lqi(ss(A,B,C,0),Q,R);
    %Eig_o = eig(A);
    %Eig_K = eig(A-B*Kgains*[C zeros(2)]);
    Eig_o = nan;
    Eig_K = nan;
end