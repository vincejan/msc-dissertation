function [A,B,C,D] = linearizeModel(f,g,x0,u0)

    x_num = length(x0);
    u_num = length(u0);
    
    f_2 = @(x) f(x(1:x_num,1),x(x_num+1:x_num+u_num,1));
    g_2 = @(x) g(x(1:x_num,1),x(x_num+1:x_num+u_num,1));
    
    Jfxu = Jacobian(f_2,[x0;u0]);    
    Jgxu = Jacobian(g_2,[x0;u0]);
    
    A = Jfxu(:,1:x_num);
    B = Jfxu(:,x_num+1:x_num+u_num);
    C = Jgxu(:,1:x_num);
    D = Jgxu(:,x_num+1:x_num+u_num);

end