function [Kgains,Eig_o,Eig_K,u0] = getGainsPl(f,g,x0,u0,DEig)

    %% trim 
    toTrim = @(u) norm(f(x0,u));
    %u0 = fminsearch(toTrim, u0);
    
    %% Linearize
    [A,B,C,~] = linearizeModel(f,g,x0,u0);
    
    DOFx = size(A,1);
    DOFy = size(C,1);
    DOFu = size(B,2);
    
    Aa = [ A    zeros(DOFx ,DOFy);
          -C zeros(DOFy,DOFy)];
    Ba = [B;zeros(DOFy,DOFu)];
    %Ca = [C zeros(DOFy,DOFy)];
    %C = diag(C);
    C( :, ~any(C,1) ) = [];
    Ca = blkdiag(eye(DOFx),C);
    %Ca = [eye(DOFx) C];
    
    
    Co = ctrb(Aa,Ba);
    % Number of uncontrollable states
    unco = length(Aa)-rank(Co)

    Ob = obsv(Aa,Ca);
    % Number of unobservable states
    unob = length(Aa)-rank(Ob);

    
    %% Place
    Kgains = place(Aa,Ba,DEig);
    
    Eig_o = eig(Aa);
    Eig_K = eig(Aa-Ba*Kgains);
end