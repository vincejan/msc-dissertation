function inty=numint(x,y)

    inty = zeros(size(y));
    %inty(1,:) = 0;
    for i=2:length(x)
        inty(:,:,i) = trapz(x(1:i),y(:,:,1:i),3);
    end


end