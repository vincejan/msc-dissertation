function [J]=Jacobian(func,x)
    % computes the Jacobian of a function, centered around x
    n =length(x);
    fx=feval(func,x);
    m =length(fx);
    dx=1.e-7;  % could be made better
    xperturb1=x;
    xperturb2=x;
    J = zeros(m,n);
    
    for i=1:n
        xperturb1(i)=xperturb1(i)+dx;
        xperturb2(i)=xperturb2(i)-dx;
        %J(:,i)=(feval(func,xperturb1)-feval(func,xperturb2))/(2*dx);
        J(:,i)=(feval(func,xperturb1)-fx)/(dx);
        xperturb1(i)=x(i);
        xperturb2(i)=x(i);
    end;
end