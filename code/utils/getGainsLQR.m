function [Kgains,Eig_o,Eig_K,u0] = getGainsLQR(f,x0,u0,Q,R)

    %% trim 
    toTrim = @(u) norm(f(x0,u));
    u0 = fminsearch(toTrim, u0);
    
    %% Linearize
    [A,B,~,~] = linearizeModel(f,x0,u0);
    
    %% LQR
    %[Kgains,~,~] = lqr(A,B,Q,R);
    [Kgains,~,~] = lqr(ss(A,B,),Q,R);
    
    Eig_o = eig(A);
    Eig_K = eig(A-B*Kgains);
end