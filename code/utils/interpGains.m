function K=interpGains(x_samp,K_samp,x)

    K = interp1(x_samp,permute(K_samp,[3 1 2]),x,'linear','extrap');
    K = permute(K,[2 3 1]);
    
end


