function [alpha,beta,Vt] = Airvelocity2AlphaBetaV(Vb)

    Vt    = norm(Vb);
    alpha = atan2(Vb(3),Vb(1));
    beta  = asin(Vb(2)/Vt);    

end