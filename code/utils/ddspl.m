% *********************************************************************
%   splines.for      p thomas, 02 july 1996
%   spline functions for hhirm aero and engine coefficients
%   per tsagi report no. 15/5dra
% *********************************************************************
%   Altered for Double Precision use 31/5/99 by
%   T.S.Richardson
%   Bristol Uni
% *********************************************************************
%   Altered for MATLAB use 04/9/00 by
%   T.S.Richardson
%   Bristol Uni
% *********************************************************************

    function [z,dz] = ddspl(x1,x2,y1,y2,x)
    
    dz = 0.0;
    if x <= x1
   	z = y1;
    elseif x>=x2
      z = y2;
    else
      dx = 1.0/(x2-x1);
      wr = (x-x1)*dx;
      wl = 1.0 - wr;
      z  = 0.5*(y2+y1+(y2-y1)*(wr-wl)*(1.0+2.0*wr*wl));
      dz = 6.0*(y2-y1)*dx*wr*wl;
    end
   

