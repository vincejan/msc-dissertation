function x_out = threshold(x,lower,upper)

    if x<lower
        x_out = lower;
    elseif x>upper
        x_out = upper;
    else
        x_out = x;
    end
end