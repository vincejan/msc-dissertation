function rads = RPM2RADs(rpm)
    rads = (360/60)*(pi/180)*rpm;
end