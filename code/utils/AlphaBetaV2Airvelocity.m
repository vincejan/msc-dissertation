function Vb = AlphaBetaV2Airvelocity(alpha, beta, Vt)

    Vb = Vt*[   cos(alpha)*cos(beta);
                           sin(beta);
                sin(alpha)*cos(beta)];
    %{
    u = sqrt((Vt^2-v^2)/(1+tan(alpha)^2));    
    v = Vt*sin(beta);    
    w = u*tan(alpha);
    Vb = [u;v;w];
    %}
end