function [Kgains,Eig_o,Eig_K,u0] = getGainsEA(f,g,x0,u0,DEig,DVec)

    %% trim 
    toTrim = @(u) norm(f(x0,u));
    u0 = fminsearch(toTrim, u0);
    
    %% Linearize
    [A,B,C,~] = linearizeModel(f,g,x0,u0);
    
    %% EA
    Kgains = ea(A,B,C,DEig,DVec);
    %Kgains = place(Aa,Ba,DEig);
    %imag(Kgains)
    Kgains=real(Kgains);
    
    Eig_o = eig(Aa);
    Eig_K = eig(Aa-Ba*Kgains);
end