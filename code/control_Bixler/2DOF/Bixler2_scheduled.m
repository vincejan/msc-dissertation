function d_x = Bixler2_scheduled(t,x,alphas,u_trims,K0,Ks,x_d)
    
    DOFy = length(x_d);
    DOFx = length(x)-DOFy;
    
    if t>5
        x_d = -x_d;
    end
    %%
    % alpha, q, theta, Vt, omega_m
    
    %u_trim = interp1(alphas,u_trims,x_d(1),'spline','extrap')';
    K = K0;
    %K = interp1(alphas,Ks,0,'spline','extrap');
    K(1,1) = interp1(alphas,Ks(:,1),x(1),'linear','extrap');
    %K(1,3) = interp1(alphas,Ks(:,3),x(1),'spline','extrap');
    
    error = x_d-x(1);
    u = -K*(x);%+u_trim;
    
    d_x = Bixler2_f2(x(1:DOFx),u);
    d_xi= error;    
    
    d_x  = [d_x;d_xi];
end