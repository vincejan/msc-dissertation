function [Ks,Phis,u_trims,alphas] = BixlerAlphaSweep(Bix_f,Bix_g,DVec_R,DEig_Q,x0,u0,alpha_min,alpha_max)

a_steps = 100;
alphas = linspace(alpha_min,alpha_max,a_steps);

DOFx = length(x0);
DOFy = 2;
%length(DEig)-DOFx;
DOFu = length(u0);
DOFK = DOFx+DOFy;

Ks = zeros(DOFu,DOFK,length(alphas));
Phis = zeros(DOFu,DOFK,length(alphas));
u_trims   = zeros(DOFu,length(alphas));

Q = diag(DEig_Q);
R = diag(DVec_R);

%% K0
%% Operating point
    
    % elevator, thrust

    %[Kgain0,~,~,u_0] = getGainsLQI(Bix_f,Bix_g,x0,u0,Q,R);
    [Kgain0,~,~,u_0] = getGainsIEA(Bix_f,Bix_g,x0,u0,DEig_Q,DVec_R);
    %[Kgain0,~,~,u_0] = getGainsPl(Bix_f,Bix_g,x0,u0,DEig_Q);
    

progress = 0;
for a = 1:length(alphas)
        
        progress = progress+1;
        if mod(progress,10)==0
            display([num2str(100*progress/(a_steps),'%10.3f') '%']);
            display(['               Alpha: ' num2str(rad2deg(alphas(a)))]);
        end
        %% Operating point
        % alpha, q, theta, Vt
        %x0 = zeros(DOFx,1);
        x0(1) = alphas(a);
        
        % elevator, thrust
        u0 = zeros(DOFu,1);

        %[Kgains,~,~,u_trims(:,a)] = getGainsLQI(Bix_f,Bix_g,x0,u0,Q,R);
        [Kgains,~,~,u_trims(:,a)] = getGainsIEA(Bix_f,Bix_g,x0,u0,DEig_Q,DVec_R);
        %[Kgains,~,~,u_trims(:,a)] = getGainsPl(Bix_f,Bix_g,x0,u0,DEig_Q);
        %Kgains;
%        Ks(a,:) = permute(Kgains(1,[1 3]),[3 1 2]);
        Ks(:,:,a) = Kgains;

end
Phis = dgs(Ks,alphas,Kgain0);
