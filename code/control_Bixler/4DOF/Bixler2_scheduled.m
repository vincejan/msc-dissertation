function d_x = Bixler2_scheduled(t,x,alphas,u_trims,K0,Ks,x_d)
    
    %if mod(t,0.1)==0
        %display(['t= ' num2str(t)])
    %end
    DOFy = length(x_d);
    DOFx = length(x)-DOFy;
    
    %x_reduced = x([3 2 5]);
    if t>5
        x_d(1) = -x_d(1);
    end
    %%
    % alpha, q, theta, Vt, omega_m
    
    %u_trim = interp1(alphas,u_trims,x_d(1),'spline','extrap')';
    
    %K = K0*0;
    
    K = interpGains(alphas,Ks,x(1));
    
    
    %K(1,1) = interpGains(alphas,Ks(1,1,:),x(1));
    %K(2,1) = interpGains(alphas,Ks(2,1,:),x(1));
    
%    K(1,1) = interp1(alphas,Ks(:,1),x(1),'linear','extrap');
    %K(1,3) = interp1(alphas,Ks(:,3),x(1),'spline','extrap');
    %K(1,end) = -1;
%x_d
    u = -K*x;
    u(1) = threshold(u(1),-2,2);
    u(2) = threshold(u(2), 0,inf);
    u(3) = threshold(u(3),-1,1);
    u(4) = threshold(u(4),-1,1);
    %display(['u= ' num2str(u')])
    %display(['x= ' num2str(x')])
    %display(Bixler2_g5(x(1:DOFx),u)')
    d_xi = x_d-Bixler2_g4(x(1:DOFx),u);
    d_x = Bixler2_f4(x(1:DOFx),u);
    
    d_x  = [d_x;d_xi];
end