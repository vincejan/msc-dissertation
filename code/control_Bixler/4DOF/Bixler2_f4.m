function d_x = Bixler2_f4(x,u)

    constraints = [ 1 0;
                    0 1;
                    1 0];
                                
    x_full = [x(1);0;0;x(2);0;0;x(3);0;x(4);0];
    u_full = zeros(7,1);
    u_full(2) = u(1);
    u_full(6) = u(2);
    u_full(7) = u(3);
    %u_full(4) = u(3);
    %u_full(5) = u(4);
    
    d_x_full = Bixler2_f(x_full,u_full,constraints);
    d_x = d_x_full([1 4 7 9],1);
end