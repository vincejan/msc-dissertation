clear_stuff = true;

if clear_stuff
    clear
    clear_stuff = true;
end

bix_enab = true;
base_enab= false;
% 5DOF - alpha, q, theta, Vt, omega_prop
%% setup gain calc
DOFx = 4;
DOFy = 2;
DOFu = 4;
DOFK = DOFx+DOFy;

%{
DEig = zeros(0,DOFx+DOFy);
DEig(end+1,:) = [-3 -4 -5 -6 -7 -8];
%DEig(end+1,:) = [-2+10i -2-10i -100];
%DEig(end+1,:) = [-3 -4 -5];
%DEig(end+1,:) = [-2+10i -2-10i -5];
%DEig(end+1,:) = [-.1+0.1i -.1-0.1i -5];
%DEig(end+1,:) = [-.1+0.1i -.1-0.1i -100];
%DEig(end+1,:) = [-1+10i -1-10i -5];



X = nan;
DVec = [1 X X X 0 0;
        X 1 X X 0 0;
        X X 1 X 0 0;
        X X X 1 0 0;
        0 0 0 0 1 0;
        0 0 0 0 0 1];
    
%}


DEig = zeros(0,DOFx+DOFy);

q_max       = deg2rad(100); %deg/sec
theta_max   = deg2rad(2); %deg
V_max       = 0.5; %m/s 
%DEig(end+1,:)=[0 1/q_max^2 0 0 1/theta_max^2 1/V_max^2];
DEig(end+1,:)=[0 1/q_max^2 0 0 1/theta_max^2 1/V_max^2];
DEig(end+1,:)=DEig(end,:)*3;
DEig(end+1,:)=[0 1/q_max^2 0 0 1/theta_max^2 10/V_max^2];

%DEig(end+1,:)=[0 1/q_max^2 0 0 100];
%DEig(end+1,:)=DEig(end,:)*3;
%DEig(end+1,:)=DEig(1,:)/3;
%DEig(end+1,:)=DEig(end,:)/3;
%DEig(end+1,:)=[0 0 0 0 10000 1000];
%DEig(end+1,:)=[1 1 0 0 0 10000 1000];

DVec = [0.001 0.001 0.001 0.001];

    
for gain_id=3:size(DEig,1)
    
    test_run = 1;

    display(['Gain id: ' num2str(gain_id)]);

%% setup sim 

    sim_id = 1;
    
    t_max = 10;
    tspan = [0 t_max];
    
    % 5DOF - alpha, q, theta, Vt, omega_prop
    x0 = zeros(DOFx+DOFy,1);
    x0(4) = 12;
    
    
    % 2DOF - elevator, thrust
    u0 = zeros(DOFu,1);
    
    % 2DOF - theta, Vt
    x_d = zeros(DOFy,1);
    x_d(1) = deg2rad(5);
    x_d(2) = 12;
    
    
%% Base sys
    if base_enab
            
        % gains
            K_base = BaseGain(DVec,DEig(gain_id,:),x0(1:DOFx),u0);
        % sim
            Sim = @(t,x) Base_cont(t,x,0,0,K_base,x_d);
            [to_bix_base,xo_bix_base] = ode45(Sim, tspan, x0);
    end
    
%% Bixler system
    if bix_enab
        
        if clear_stuff
            % gains
                alpha_min = deg2rad(-10);
                alpha_max = deg2rad(18);
                [Ks_bix,Phis_bix,u_trims_bix,alphas_bix] = BixlerAlphaSweep(@Bixler2_f4,@Bixler2_g4,DVec,DEig(gain_id,:),x0(1:DOFx),u0,alpha_min,alpha_max);
                K0_bix = interpGains(alphas_bix,Ks_bix,0);
        end
        % sim SGS
            display('Sim SGS');
            Sim = @(t,x) Bixler2_scheduled(t,x,alphas_bix,u_trims_bix,K0_bix,Ks_bix,x_d);
            [to_bs,xo_bs] = ode45(Sim, tspan, x0);
            %Ko_bs = repmat(K0_bix,length(xo_bs),1);
            %Ko_bs(:,1) = interpGains(alphas_bix,Ks_bix(:,1),xo_bs(:,1));
            %uo_bs = -sum(xo_bs.*Ko_bs,2);
        % sim DGS
            display('Sim DGS');
            Sim = @(t,x) Bixler2_scheduled(t,x,alphas_bix,u_trims_bix,K0_bix,Phis_bix,x_d);
            [to_bd,xo_bd] = ode45(Sim, tspan, x0);
            %Ko_bd = repmat(K0_bix,length(xo_bd),1);
            %Ko_bd(:,1) = interpGains(alphas_bix,Phis_bix(:,1),xo_bd(:,1));
            %uo_bd = -sum(xo_bd.*Ko_bd,2);
    end
%% plot
    c = lines;
    %% Bixler 
    if bix_enab
        %% gains
        print_gains = false;
        if print_gains
            figure('units','normalized','position',[.0 .0 .75 .75])
            legendInfo = {};

            %alphas_bix_plot= deg2rad(linspace(-10,30,1000));
            %Ks_bix_plot = interp1(alphas_bix,Ks_bix(:,1),alphas_bix_plot','linear','extrap');
            %Phis_bix_plot = interp1(alphas_bix,Phis_bix(:,1),alphas_bix_plot','linear','extrap');


            plots = 0;
            for k = 2:2%DOFu
                for l = 1:DOFK
                    plots = plots+1;
                    plot(rad2deg(alphas_bix),permute(Ks_bix(k,l,:),[3 1 2]),'x-','Color',c(plots,:),'LineWidth',1)
                    %plot(rad2deg(alphas_bix_plot),Ks_bix_plot,'-','Color',c(plots,:));
                    hold on
                    legendInfo{plots} = ['K - ' num2str(k) ',' num2str(l)]; %num2str(ceil(k/DOFx)) num2str(mod(k,DOFu))]; 
                end
            end
%{
            for k = 1:DOFu
                for l = 1:DOFK
                    plots = plots+1;
                    plot(rad2deg(alphas_bix),permute(Phis_bix(k,l,:),[3 1 2]),'x-','Color',c(plots,:),'LineWidth',1)
                    %plot(rad2deg(alphas_bix_plot),Phis_bix_plot,'-','Color',c(plots,:));
                    hold on
                    legendInfo{plots} = ['\Phi - ' num2str(k) ',' num2str(l)]; %num2str(ceil(k/DOFx)) num2str(mod(k,DOFu))]; 
                end
            end
%}
            grid on
            xlabel('\alpha (deg)')
            ylabel('gain magnitude')
            legend(legendInfo)
            print(['alphasweep_bix_' num2str(gain_id)],'-dpng')

            figure('units','normalized','position',[.0 .0 .75 .75])
            plot(rad2deg(alphas_bix),u_trims_bix(:,:,1),'LineWidth',1)
            grid on
            xlabel('\alpha (deg)')
            ylabel('Magnitude')
            legend('u_0');
            print(['u0_bix_' num2str(gain_id)],'-dpng')

        end
        %% sim
        c = lines;
        figure('units','normalized','position',[.0 .0 .75 .75],'Visible', 'off');
        hold on
        grid on

        % baseline
            %plot(to_bix_base,rad2deg(xo_bix_base(:,1)),':','Color',c(1,:),'LineWidth',1)
            %plot(to_bix_base,rad2deg(xo_bix_base(:,2)),':','Color',c(2,:))

        % sys SGS
            %plot(to_bs,rad2deg(xo_bs(:,1)),'--','Color',c(1,:),'LineWidth',1)
            plot(to_bs,rad2deg(xo_bs(:,[1])),'--','Color',c(1,:),'LineWidth',1)
            plot(to_bs,rad2deg(xo_bs(:,[3])),'--','Color',c(2,:),'LineWidth',1)
            plot(to_bs,(xo_bs(:,[4])),'--','Color',c(3,:),'LineWidth',1)
            plot(to_bs,rad2deg(xo_bs(:,3)-xo_bs(:,1)),'--','Color',c(4,:),'LineWidth',1)
            %plot(to_bs,rad2deg(xo_bs(:,2)),'--','Color',c(2,:))

        % sys DGS
            %plot(to_bd,rad2deg(xo_bd(:,1)),'-.','Color',c(1,:),'LineWidth',1)
            plot(to_bd,rad2deg(xo_bd(:,[1])),'-.','Color',c(1,:),'LineWidth',1)
            plot(to_bd,rad2deg(xo_bd(:,[3])),'-.','Color',c(2,:),'LineWidth',1)
            plot(to_bd,(xo_bd(:,[4])),'-.','Color',c(3,:),'LineWidth',1)
            plot(to_bd,rad2deg(xo_bd(:,3)-xo_bd(:,1)),'-.','Color',c(4,:),'LineWidth',1)
            %plot(to_bd,rad2deg(xo_bd(:,2)),'-.','Color',c(2,:))



        xlabel('time (s)')
        ylabel('magnitude')
        legend('\alpha_{SGS} (deg)', '\theta_{SGS} (deg/s)','Vt_{SGS} (m/s)','\gamma_{SGS}','\alpha_{DGS} (deg)', '\theta_{DGS} (deg/s)','Vt_{DGS} (m/s)','\gamma_{DGS}')
        %legend('\alpha (deg)','q (deg/s)','\theta (deg)', 'V_t')
        %legend('\alpha (deg)','\theta (deg)', 'V_t')
        grid on
        print(['sim_result_bix_' num2str(gain_id) '_' num2str(sim_id)],'-dpng')
        %title(['Bixler test run for \lambda\in\{' num2str(DEig) '\}']);
        
        %{
        
        figure('units','normalized','position',[.0 .0 .75 .75])
        hold on
        grid on

        % u
            %plot(to_bs,(uo_bs(:,1)),'--','Color',c(1,:))
            plot(to_bd,(uo_bd(:,1)),'-','Color',c(1,:),'LineWidth',1)



        xlabel('time (s)')
        ylabel('magnitude')
        %legend('\alpha_{ideal} (deg)', 'q_{ideal} (deg/s)','\alpha_{SGS} (deg)', 'q_{SGS} (deg/s)','\alpha_{DGS} (deg)', 'q_{DGS} (deg/s)')
        legend('u_{DGS}')
        grid on
        print(['sim_result_bix_u_' num2str(gain_id) '_' num2str(sim_id)],'-dpng')
        %title(['Pendulum test run for \lambda\in\{' num2str(DEig) '\}']);
        %}
    end
    
    save(['run_' num2str(gain_id) '_' num2str(sim_id)])
    %return
    close all
end    
%% save    
    !mv ./*.png ./Results2/
    !mv ./*.mat ./Results2/
    
    !notify-send MATLAB done