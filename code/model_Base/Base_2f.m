function dx=Base_2f(x,u)
    
    A = [0 1; -10 -1];
    B = [0;1];
    %display(num2str(x'))
    %display(num2str(u'))
    dx = A*x+B*u;
    
end